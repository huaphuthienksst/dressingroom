const renderItem = (data) => {
  let dataHTML = "";
  console.log(data);
  for (let i in data) {
    dataHTML += `
      <div class="col-3 d-flex flex-column">
          <img src="${data[i].imgSrc_jpg}" style="width:100%">
          <p  style="font-weight:bold; font-size:1.5rem ; text-align:center;margin-top:0.5rem">${data[i].name}</p>
          <button onclick="handleShowItem('${data[i].type}','${data[i].id}')" class="btn btn-outline-info">Thử dồ</button>
      </div>
    `;
  }
  document.getElementById("list-item-row").innerHTML = dataHTML;
};
const handleRenderItem = (type) => {
  let listItem = [];
  for (let i in listChosen.arr) {
    if (listChosen.arr[i].type == type) {
      listItem.push(listChosen.arr[i]);
    }
  }
  renderItem(listItem);
};
const handleShowItem = (type, id) => {
  for (let i in listChosen.arr) {
    if (listChosen.arr[i].id == id) {
      console.log(listChosen.arr[i].imgSrc_png);
      if (type === "topclothes") {
        document.getElementById(
          "showTopclothes"
        ).style.background = `url("${listChosen.arr[i].imgSrc_png}")`;
        return;
      } else if (type === "botclothes") {
        document.getElementById(
          "showBotclothes"
        ).style.background = `url("${listChosen.arr[i].imgSrc_png}")`;
        return;
      } else if (type === "shoes") {
        document.getElementById(
          "showShoes"
        ).style.background = `url("${listChosen.arr[i].imgSrc_png}")`;
        return;
      } else if (type === "handbags") {
        document.getElementById(
          "showHandbags"
        ).style.background = `url("${listChosen.arr[i].imgSrc_png}")`;
        return;
      } else if (type === "necklaces") {
        document.getElementById(
          "showNecklaces"
        ).style.background = `url("${listChosen.arr[i].imgSrc_png}")`;
        return;
      } else if (type === "hairstyle") {
        document.getElementById(
          "showHairstyle"
        ).style.background = `url("${listChosen.arr[i].imgSrc_png}")`;
        return;
      } else if (type === "background") {
        document.getElementById(
          "showBackground"
        ).style.backgroundImage = `url("${listChosen.arr[i].imgSrc_png}")`;
        return;
      }
    }
  }
};
handleRenderItem("topclothes");
